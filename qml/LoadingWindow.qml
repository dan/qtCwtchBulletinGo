import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 2.2

Window {
    //id: loadingWindow
    visible: true
    title: "Cwtch Bulletin"
    color: "#ffffff"
    width: 349
    height: 145

    Column {
        Image {
            source: "../assets/cwtch-logo.png"
            width: 349
            height: 125
        }
        ProgressBar {
               id: loadingProgressBar
               width: parent.width
               indeterminate: false
               value: 0
        }
        Text {
            id: loadingProgressText
            text: ""
        }
    }

    Connections {
        target: cwtchApp

        onTorStatusProgressChanged: loadingProgressBar.value = cwtchApp.torStatusProgress/100
        onTorStatusSummaryChanged: loadingProgressText.text = cwtchApp.torStatusSummary
    }
}
