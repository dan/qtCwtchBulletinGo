import QtQuick 2.0

Rectangle {
    color: "#4a3458"

    property alias model: list.model

    ListView {
        id: list
        height: parent.height
        width: parent.width
        clip: true

        highlight: Rectangle { color: "#ffffff"; radius: 5 }

        delegate: Component {
            id: groupsDelegate

            Item {
                width: ListView.view.width
                height: 40

                Column {
                    Text { color: "#a0a0a0"; text: '<b>' + title + '</b>' }
                    Text { color: "#a0a0a0"; text: groupid }
                }

                MouseArea {
                    id: mouse_area1
                    z: 1
                    hoverEnabled: false
                    anchors.fill: parent
                    onClicked: {
                        list.currentIndex = index
                    }
                }
            }
        }

        section.property: "type"
        section.delegate: Component {
                   id: groupsSectionDelegate

                   Item {
                       width: ListView.view.width
                       height: 20
                       //text: section

                       Column {
                           Text { color: "#a0a0a0"; text: section + " (" + section.count + ")"}
                       }
                   }
               }


    }
}

