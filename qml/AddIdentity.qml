import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Controls 1.4

//import CustomQmlTypes 1.0

Window {
    visible: false
    id: addIdentityWindow

    width: 600
    //minimumWidth: 600
    height: 400
    //minimumHeight: 400
    title: "Add Identity"

    signal closed
    onVisibleChanged: if (!visible) closed()

    Item {

        anchors.left: parent.left
        anchors.leftMargin: 200


        Column {
            Text { text: "Name:" }
            TextField { id: nameText }
            TextField { id: password; echoMode: TextInput.Password }
            TextField { id: passconfirm; echoMode: TextInput.Password }
            Text { id: passError; visible: false; text: "Passwords do not match"; color: "red" }
            Button { text: "Create"; isDefault: true;
                onClicked: {
                    if (password.text !== passconfirm.text) {
                        passError.visible = true
                    } else {
                        passError.visible = false
                        cwtchApp.identityCreate(nameText.text, password.text)
                        addIdentityWindow.visible = false
                    }
                }
            }
        }
    }

}
