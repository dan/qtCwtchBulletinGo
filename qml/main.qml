import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.0

// Root non-graphical object providing window management and other logic.
QtObject {
    id: root

    property MainWindow mainWindow: MainWindow {
        onVisibleChanged: if (!visible) Qt.quit()
    }

    property LoadingWindow loadingWindow: LoadingWindow { visible: true }

    property Timer timer: Timer {
           id: timer
       }

   function delay(delayTime, cb) {
       timer.interval = delayTime;
       timer.repeat = false;
       timer.triggered.connect(cb);
       timer.start();
   }

    property list<QtObject> commObjs: [
        Connections {
            target: cwtchApp

            onTorStatusProgressChanged: { if (cwtchApp.torStatusProgress === 100) {
                   delay(500, function() {
                        loadingWindow.visible = false
                        mainWindow.visible = true
                   })
                }
            }

        }
    ]
}



