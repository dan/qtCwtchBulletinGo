import QtQuick 2.9
import QtQuick.Window 2.2

Window {
    //visible: false
    id: mainWindow
    width: 1024
    height: 800
    title: qsTr("Bulletin")

    ProfilesColumn {
        id: profilesColumn

        anchors.top: parent.top
        anchors.left: parent.left
        height: parent.height
        width: 100
    }

   GroupsColumn {
       id: groupsColumn
       model: groups

       anchors.top: parent.top
       anchors.left: profilesColumn.right
       height: parent.height
       width: 250
   }

    /*ListModel {
        id: profiles

        ListElement {name: "alice";  onion: "710829b408e8b368273cbc20b07f1c0d"; pcolor: "#ff0000"}
        ListElement {name: "bob"; onion: "a219b9740fc76367833cbc20b07d1cee"; pcolor: "#00ff00" }
        ListElement {name: "carol";  onion: "930829b408e8b364563cbc20b07a6560"; pcolor: "#0000ff" }
        ListElement {name: "+";  onion: "930829b408e8b364563cbc20b07a6560"; pcolor: "#a0a0a0" }
    }*/

    ListModel {
        id: groups

        ListElement {type: "group"; title: "Work Friends"; groupid: "710829b408e8b368273cbc20b07f1c0d"}
        ListElement {type: "group"; title: "Cwtch Dev"; groupid: "a219b9740fc76367833cbc20b07d1cee" }
        ListElement {type: "group"; title: "Gaming Squad"; groupid: "930829b408e8b364563cbc20b07a6560" }

        ListElement {type: "bulletin"; title: "Vancouver Listings"; groupid: "890a47403e87368273cbc2ebf7f1cdc"}
        ListElement {type: "bulletin"; title: "Game Discussions"; groupid: "cc45892408123879273ec2a435cc4234"}
    }

    /*Connections {
        target: cwtchApp

        onIdentitiesChanged: { ProfilesColumn.model = cwtchApp.identities ;
            profilesColumn.update();
        }
    }*/


}
