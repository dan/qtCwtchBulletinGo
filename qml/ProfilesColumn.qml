import QtQuick 2.0

Rectangle {

    color: "#381F47"

    ListView {
        id: list
        //height: parent.height     
        width: parent.width
        anchors.fill: parent

        ListModel {
            id: identitiesModel
            ListElement { name: "unlock"; onion: ""; color: "#af921d"; buttonType: 1}
            ListElement { name: "add"; onion: ""; color: "#e0e0e0"; buttonType: 2}
        }
        model: identitiesModel

        highlight: Item { Rectangle { width: 4; height: 100; color: "white"; radius: 2} }

        //highlight: highlightBar
        //highlightFollowsCurrentItem: false

        delegate: profileDelegate

        Connections {
            target: cwtchApp

            onIdentityAddList: function (name, onion, color, buttonType) {
                console.log("on Identity-Add-List(" + name + "," + onion +","+ color+ "," + buttonType+")")
                identitiesModel.insert(identitiesModel.count-2, {
                                          "name": name,
                                          "onion": onion,
                                          "color": color,
                                          "buttonType": buttonType
                                      })
            }
        }
    }

    Component {
        id: profileDelegate

        Item {
            height: 100
            width: 100

            Column {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.leftMargin: 10

                Rectangle { y: 10; width: 80; height: 80; color: model.color }
                Text { color: "#a0a0a0"; text: name }
            }


            MouseArea {
              id: profile_mousearea
              z: 1
              hoverEnabled: false
              anchors.fill: parent
              onClicked: {
                  list.currentIndex = index

                 // var addIWin = AddIdentity{ }
                    console.log("onClicked " +  model.name + " type " + model.buttonType)
                  if (model.buttonType === cwtchApp.typeAdd) {
                      var component = Qt.createComponent("AddIdentity.qml")
                      var object = component.createObject(root)
                      object.show()
                      //object.closed.connect(function() { object.destroy() })
                  } else
                  if (model.ButtonType === cwtchApp.typeIdentity) {
                    cwtchApp.identityClicked(model.name + " " + model.buttonType)
                  }
              }
          }
      }

  }


}

