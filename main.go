package main

import (
	"os"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/gui"
	"github.com/therecipe/qt/qml"
	"git.openprivacy.ca/openprivacy/asaur"
	"cwtch.im/cwtch/app"
	"os/exec"
	"log"
	"os/user"
	"path"
	"time"
	"strconv"
	"fmt"
)

const (
	identity = iota
	unlock
	add
)


type QmlCwtchApp struct {
	core.QObject

	_ int `property:"torStatusProgress"`
	_ string `property:"torStatusSummary"`

	_ int `property:"typeIdentity"`
	_ int `property:"typeUnlock"`
	_ int `property:"typeAdd"`

	_ func(string) `signal:"identityClicked,auto"`
	_ func(string, string) `signal:"identityCreate,auto"`
	_ func(string, string, string, int) `signal:"identityAddList"`

	_ func() `constructor:"init"`
}

func (ca *QmlCwtchApp) init() {
	ca.SetTypeIdentity(identity)
	ca.SetTypeUnlock(unlock)
	ca.SetTypeAdd(add)
}

func (ca *QmlCwtchApp) identityClicked(s string) {
	fmt.Println("clickIdentity: '" + s + "'")
}

func (ca *QmlCwtchApp) identityCreate(name, password string) {
	fmt.Printf("identityCreate(%v)\n", name)
	peer, err := cwtchApp.CreatePeer(name, password)
	if err != nil {
		fmt.Printf("ERROR creating new identity: %v\n", err)
		return
	}

	ca.IdentityAddList(peer.GetProfile().Name, peer.GetProfile().Onion, "red", identity)
}


var cwtchApp app.Application

func InitCwtch(engine *qml.QQmlApplicationEngine, qCwtchApp *QmlCwtchApp) error {

	torPath, err := exec.LookPath("tor")
	if err != nil {
		log.Fatal("tor could not be found on this system. Please install it in the system $PATH")
	}

	usr, err := user.Current()
	if err != nil {
		log.Fatalf("\nError: could not load current user: %v\n", err)
	}

	doneChan := make(chan bool)

	go func() {
		for {
			time.Sleep(100 * time.Millisecond)
			rawStatus, _ := asaur.GetInfo("localhost:9051", "tcp4", "", "status/bootstrap-phase")
			status := asaur.ParseBootstrapPhase(rawStatus)
			progress, _ := strconv.Atoi(status["PROGRESS"])
			qCwtchApp.SetTorStatusProgress(progress)

			qCwtchApp.SetTorStatusSummary(status["SUMMARY"])

			if status["TAG"] == "done" {
				break
			}
		}
		doneChan <- true
	}()

	cwtchApp, err = app.NewApp(path.Join(usr.HomeDir, ".cwtch"), torPath)
	if err != nil {
		log.Fatalf("Error initializing application: %v", err)
	}



	/*identityList.Add(*NewIdentityListItemValues("alice", "1234567890abcdef", "red", identity))
	identityList.Add( *NewIdentityListItemValues( "unlock", "", "#af921d", unlock))
	identityList.Add( *NewIdentityListItemValues("add", "", "#e0e0e0", add))*/

	//cwtchApp.i( IdentityListItem{"alice", "1234567890abcdef", "red", identity})


	<-doneChan

	return nil
}

func main() {

	// enable high dpi scaling
	// useful for devices with high pixel density displays
	// such as smartphones, retina displays, ...
	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)

	// needs to be called once before you can start using QML
	gui.NewQGuiApplication(len(os.Args), os.Args)

	// use the material style
	// the other inbuild styles are:
	// Default, Fusion, Imagine, Universal
	//quickcontrols2.QQuickStyle_SetStyle("Material")

	var qCwtchApp = NewQmlCwtchApp(nil)

	//qCwtchApp.SetIdentities(append(qCwtchApp.Identities(), alice))*/


	// create the qml application engine
	engine := qml.NewQQmlApplicationEngine(nil)
	//engine.QmlRegisterType()

	engine.RootContext().SetContextProperty("cwtchApp", qCwtchApp)

	// load the embeeded qml file
	// created by either qtrcc or qtdeploy
	//engine.Load(core.NewQUrl3("qrc:/qml/LoadingWindow.qml", 0))
	//engine.Load(core.NewQUrl3("qrc:/qml/MainWindow.qml", 0))

	// you can also load a local file like this instead:
	//engine.Load(core.QUrl_FromLocalFile("./qml/main.qml"))
	engine.Load(core.NewQUrl3("qrc:/qml/main.qml", 0))
	engine.Load(core.NewQUrl3("qrc:/qml/AddIdentity.qml", 0))

	go InitCwtch(engine, qCwtchApp)
	// start the main Qt event loop
	// and block until app.Exit() is called
	// or the window is closed by the user
	gui.QGuiApplication_Exec()

	// Cleanup
	cwtchApp.Shutdown()
}

